FROM node:10
LABEL maintainer="Per Karlsson <pkarl.home@gmail.com>"
COPY script.sh /tmp
RUN chmod +x /tmp/script.sh
RUN sh /tmp/script.sh

WORKDIR /app
COPY ./nginx.conf /nginx.conf